<?php

namespace App\Http\Controllers;
use App\Pegawai;
use App\Laporan;
use Illuminate\Http\Request;
use PDF;
class LaporController extends Controller
{
    public function index()
    {
        return view('lapor');
    }
    public function index_pos()
    {
        return view('pos');
    }
    public function index_drop()
    {
        return view('drop');
    }
    public function index_sms()
    {
        return view('sms');
    }
    public function index_email()
    {
        return view('email');
    }

    public function index_admin(request $request)
    {
        $halaman='Daftar Laporan';
        if($request->progres==''){
            return redirect('admin?progres=0');
        }else{
            $progres=$request->progres;
        }
        return view('admin.index',compact('progres','halaman'));
    }

    public function index_tiket(request $request){
        $id=explode('@',$request->NOTIKET);
        $data=Laporan::where('id',$id[0])->first();
        return view('tiket',compact('data'));
    }

    public function index_pantau(request $request){
        error_reporting(0);
        $id=$request->ID;
        $data=Laporan::with(['pegawai'])->where('tiket',$id)->first();
        return view('pantau',compact('data'));
    }

    public function proses(request $request,$id){
        $data=Laporan::with(['pegawai'])->where('id',$id)->first();
        echo'
                <input type="hidden" name="id" value="'.$data['id'].'">
                <div class="form-group">
                    <label>Judul</label><br>
                    <textarea class="form-control" disabled>'.$data['dugaan'].'</textarea>
                    <label>Deskripsi</label><br>
                    <textarea class="form-control" disabled>'.$data['deskripsi'].'</textarea>
                    <label>Nama Pelanggar</label><br>
                    <input type="text" class="form-control" disabled value="'.$data->pegawai['empname'].'">
                    <label>NIK Pelanggar</label><br>
                    <input type="text" class="form-control" disabled value="'.$data->pegawai['empnik'].'">
                    <label>Jabatan</label><br>
                    <input type="text" class="form-control" disabled value="'.$data->pegawai['emp_t503t_ptext'].'">
                    <label>Persetujuan</label><br>
                    <select class="form-control" onchange="pilih_persetujuan(this.value)" name="progres">
                        <option value="">Pilih Persetujuan ----</option>
                        <option value="1">Bisa Diproses</option>
                        <option value="X">Tidak Bisa Diproses</option>
                    </select>
                    <div id="pic">
                        <label>Investigator</label>
                        <select class="form-control"  name="investigator_id">
                            <option value="">Pilih ----</option>';
                            foreach(investigator() as $inv){
                                echo'<option value="'.$inv['id'].'">'.$inv['name'].'</option>';
                            }
                            echo'
                        </select>
                    </div>
                    <div id="alasan">
                        <label>Alasan</label>
                        <textarea name="alasan" class="form-control" rows="4"></textarea>
                    </div>
                </div>
        ';
        echo'
            <script>
                $("#pic").hide();
                $("#alasan").hide();
            </script>
        ';
    }

    public function keputusan(request $request,$id){
        $data=Laporan::with(['pegawai'])->where('id',$id)->first();
        echo'
                <input type="hidden" name="id" value="'.$data['id'].'">
                <div class="form-group">
                    <label>Keputusan</label><br>
                    <select class="form-control"  name="progres">
                        <option value="">Pilih Keputusan ----</option>
                        <option value="2">Terbukti Bersalah Dan Sudah Dieksekusi</option>
                        <option value="T">Tidak Terbukti Bersalah</option>
                    </select>
                    
                </div>
        ';
        echo'
            <script>
                $("#pic").hide();
                $("#alasan").hide();
            </script>
        ';
    }

    public function sanksi(request $request,$id){
        $data=Laporan::with(['pegawai'])->where('id',$id)->first();
        echo'
                <input type="hidden" name="id" value="'.$data['id'].'">
                <div class="form-group">
                    <label>File BAP</label><br>
                    <input type="file" class="form-control"  name="file">
                    
                </div>
        ';
        echo'
            <script>
                $("#pic").hide();
                $("#alasan").hide();
            </script>
        ';
    }
    public function view_data_admin(request $request){
        $cek=strlen($request->name);
        echo'
            
            <table class="table table-hover" style="width:98%;margin-left:1%">
                <tr>
                    <th width="5%">No '.$request->progres.'</th>
                    <th width="15%">Tiket</th>
                    <th width="15%">Waktu</th>
                    <th>Keterangan</th>
                    <th width="20%">Pelanggar</th>
                    <th width="20%">Analisa</th>
                    <th width="10%">Act</th>
                </tr>';
                if($cek>0){
                    if($request->progres=='2'){
                        $data=Laporan::with(['pegawai','investigator'])->whereIn('progres',['2','4'])->where('tiket','LIKE','%'.$request->name.'%')->orderBy('id','Desc')->get();
                    }else{
                        $data=Laporan::with(['pegawai','investigator'])->where('progres',$request->progres)->where('tiket','LIKE','%'.$request->name.'%')->orderBy('id','Desc')->get();
                    }
                    
                }else{
                    if($request->progres=='2'){
                        $data=Laporan::with(['pegawai','investigator'])->whereIn('progres',['2','4'])->orderBy('id','Desc')->paginate(200);
                    }else{
                        $data=Laporan::with(['pegawai','investigator'])->where('progres',$request->progres)->orderBy('id','Desc')->paginate(200);
                    }
                    
                }
                
                foreach($data as $no=>$o){
                    echo'    
                        <tr>
                            <td class="ttd">'.($no+1).'</td>
                            <td class="ttd">'.$o['tiket'].'</td>
                            <td class="ttd"><b>Create:</b><br>'.$o['tgl_create'].'<br><b>Verifikasi:</b><br>'.$o['tgl_investigasi'].'<br><b>Investigasi:</b><br>'.$o['tgl_keputusan'].'</td>
                            <td class="ttd"><b>Judul:</b><br>'.$o['dugaan'].'<br><b>Deskripsi:</b><br>'.$o['deskripsi'].'</td>
                            <td class="ttd"><b>Nama:</b><br>'.$o->pegawai['empname'].'<br><b>NIK:</b><br>'.$o['nik'].'<br><b>Jabatan:</b><br>'.$o->pegawai['emp_t503t_ptext'].'</td>
                            <td class="ttd"><b>Investigator:</b><br>'.$o->investigator['name'].'<br><b>Proses:</b><br>'.cek_proses($o['progres']).'<br><b>Alasan:</b><br>'.$o['alasan'].'</td>
                            <td class="ttd" style="text-align:center">
                             <a href="'.url('_file/'.$o['file']).'" target="_blank"><span class="btn btn-warning"><i class="fa fa-clone"></i></span></a><br><br>
                            ';
                            if($o['progres']=='0'){
                                echo'<span class="btn btn-success btn-xs" onclick="proses('.$o['id'].')"><i class="fa fa-pencil"></i> Proses</span>';
                            }elseif($o['progres']=='1'){
                                echo'<span class="btn btn-primary btn-xs" onclick="keputusan('.$o['id'].')"><i class="fa fa-legal"></i> Keputusan</span>';
                                
                            }elseif($o['progres']=='2'){
                                echo'<span class="btn btn-success btn-xs" onclick="sanksi('.$o['id'].')"><i class="fa fa-cloud-upload"></i> Upload BAP</span>';
                            }elseif($o['progres']=='X'){
                                
                            }elseif($o['progres']=='4'){
                                echo'<a href="'.url('_file/'.$o['file_bap']).'" target="_blank"><span class="btn btn-success"><i class="fa fa-clone"></i> BAP</span></a>';
                            }else{

                            }
                                
                            echo'
                            </td>
                         </tr>';
                }
         echo'
            </table>
        ';
    }

    public function view_data(request $request){
        $cek=strlen($request->name);
        echo'
            
            <table class="table table-hover" style="width:98%;margin-left:1%">
                <tr>
                    <th width="5%">No</th>
                    <th>Nama</th>
                    <th width="30%">Unit Kerja</th>
                    <th width="10%">Act</th>
                </tr>';
                if($cek>0){
                    $data=Pegawai::select('empnik','empname','emp_cskt_ltext')->where('empname','LIKE','%'.$request->name.'%')->orWhere('empnik','LIKE','%'.$request->name.'%')->groupBy('empnik','empname','emp_cskt_ltext')->orderBy('empname','Asc')->get();
                }else{
                    $data=Pegawai::select('empnik','empname','emp_cskt_ltext')->groupBy('empnik','empname','emp_cskt_ltext')->orderBy('empname','Asc')->paginate(100);
                }
                
                foreach($data as $no=>$o){
                    echo'    
                        <tr>
                            <td class="ttd">'.($no+1).'</td>
                            <td class="ttd">'.$o['empname'].'</td>
                            <td class="ttd">'.$o['emp_cskt_ltext'].'</td>
                            <td class="ttd">
                                <span class="btn btn-success btn-xs" onclick="pilih('.$o['empnik'].',`'.$o['empname'].'`)"><i class="fa fa-check"></i> Pilih</span>
                              </td>
                         </tr>';
                }
         echo'
            </table>
        ';
    }

    public function simpan(request $request){
        error_reporting(0);
        if (trim($request->name) == '') {$error[] = '<li class="lii"><i class="fa fa-times-circle"></i> Isi Nama Pelapor terlebih dahulu</li>';}
        if (trim($request->alamat) == '') {$error[] = '<li class="lii"><i class="fa fa-times-circle"></i> Isi Alamat Pelapor terlebih dahulu</li>';}
        if (trim($request->dugaan) == '') {$error[] = '<li class="lii"><i class="fa fa-times-circle"></i> Isi Dugaan Pelanggaran terlebih dahulu</li>';}
        if (trim($request->deskripsi) == '') {$error[] = '<li class="lii"><i class="fa fa-times-circle"></i> Isi Deskripsi terlebih dahulu</li>';}
        if (trim($request->sts) == '') {$error[] = '<li class="lii"><i class="fa fa-times-circle"></i> Pilih Identitas Pelanggar terlebih dahulu</li>';}
        if (isset($error)) {echo '<ul style="padding: 0px;">'.implode('', $error).'</ul>';} 
        else{
            $tik=tiket();
            $cek=Laporan::where('kategori',$tik)->count();
            if($cek>0){
                $tiket=$tik.'-'.($cek+1);
            }else{
                $tiket=$tik.'-1';
            }

           if($request->sts==1){
               if($request->nik==''){
                    echo '<ul style="padding: 0px;"><li class="lii"><i class="fa fa-times-circle"></i> Pilih nama pelanggar</li></ul>';
               }else{
                  if($request->file == '') {
                        $data               =   new Laporan;
                        $data->kategori     =   $tik;
                        $data->tiket        =   $tiket;
                        $data->name         =   $request->name;
                        $data->alamat         =   $request->alamat;
                        $data->dugaan       =   $request->dugaan;
                        $data->deskripsi    =   $request->deskripsi;
                        $data->nik          =   $request->nik;
                        $data->sts          =   $request->sts;
                        $data->progres      =   0;
                        $data->tgl_create      =   date('Y-m-d H:i:s');
                        $data->save();

                        echo'ok|'.$data['id'].'|'.$data['tiket'];
                  }else{
                        $file=$_FILES['file']['name'];
                        $size=$_FILES['file']['size'];
                        $asli=$_FILES['file']['tmp_name'];
                        $ukuran=getimagesize($_FILES["file"]['tmp_name']);
                        $typee=explode('/',$_FILES['file']['type']);
                        $tipe=explode('.',$_FILES['file']['name']);
                        $filename=date('Ymdhis').'.'.$typee[1];
                        $lokasi='_file/';
                        if(($typee[0]=='image' || $typee[1]=='pdf') && $size<=1048576){
                            if(move_uploaded_file($asli, $lokasi.$filename)){
                                $data               =   new Laporan;
                                $data->kategori     =   $tik;
                                $data->tiket        =   $tiket;
                                $data->name         =   $request->name;
                                $data->alamat         =   $request->alamat;
                                $data->dugaan       =   $request->dugaan;
                                $data->deskripsi    =   $request->deskripsi;
                                $data->nik          =   $request->nik;
                                $data->file          =   $filename;
                                $data->sts          =   $request->sts;
                                $data->progres      =   0;
                                $data->tgl_create      =   date('Y-m-d H:i:s');
                                $data->save();

                                echo'ok|'.$data['id'].'|'.$data['tiket'];
                            }
                        }else{
                            echo '<ul style="padding: 0px;"><li class="lii"><i class="fa fa-times-circle"></i> File Harus berformat gambar/pdf dan maximal size 1mb </li></ul>';
                        }
                    }
               }
                
           }else{
                if($request->file == '') {
                    $data               =   new Laporan;
                    $data->kategori     =   $tik;
                    $data->tiket        =   $tiket;
                    $data->name         =   $request->name;
                    $data->dugaan       =   $request->dugaan;
                    $data->deskripsi    =   $request->deskripsi;
                    $data->alamat         =   $request->alamat;
                    $data->nik          =   $request->nik;
                    $data->sts          =   $request->sts;
                    $data->progres      =   0;
                    $data->tgl_create      =   date('Y-m-d H:i:s');
                    $data->save();

                    echo'ok|'.$data['id'].'|'.$data['tiket'];
                }else{
                    $file=$_FILES['file']['name'];
                    $size=$_FILES['file']['size'];
                    $asli=$_FILES['file']['tmp_name'];
                    $ukuran=getimagesize($_FILES["file"]['tmp_name']);
                    $tipe=explode('.',$_FILES['file']['name']);
                    $typee=explode('/',$_FILES['file']['type']);
                    $filename=date('Ymdhis').'.'.$typee[1];
                    $lokasi='_file/';
                    if(($typee[0]=='image' || $typee[1]=='pdf') && $size<=1048576){
                        if(move_uploaded_file($asli, $lokasi.$filename)){
                            $data               =   new Laporan;
                            $data->kategori     =   $tik;
                            $data->tiket        =   $tiket;
                            $data->name         =   $request->name;
                            $data->dugaan       =   $request->dugaan;
                            $data->alamat         =   $request->alamat;
                            $data->deskripsi    =   $request->deskripsi;
                            $data->sts          =   $request->sts;
                            $data->file          =   $filename;
                            $data->tgl_create      =   date('Y-m-d H:i:s');
                            $data->progres      =   0;
                            $data->save();
                            
                            echo'ok|'.$data['id'].'|'.$data['tiket'];
                        }
                    }else{
                        echo '<ul style="padding: 0px;"><li class="lii"><i class="fa fa-times-circle"></i> File Harus berformat gambar/pdf dan maximal size 1mb </li></ul>';
                    }
                }
           }
            

            
            
        }
    }

    public function proses_investigasi(request $request){
        if (trim($request->progres) == '') {$error[] = '<li class="lii"><i class="fa fa-times-circle"></i> Pilih Persetujuan terlebih dahulu</li>';}
        if (isset($error)) {echo '<ul style="padding: 0px;background:aqua">'.implode('', $error).'</ul>';} 
        else{
            if($request->progres==1){
                if($request->investigator_id==''){
                    echo '<ul style="padding: 0px;background:aqua"><li class="lii"><i class="fa fa-times-circle"></i> Pilih Investigator terlebih dahulu</li></ul>';
                }else{
                    $data               =   Laporan::find($request->id);
                    $data->investigator_id         =   $request->investigator_id;
                    $data->progres       =   $request->progres;
                    $data->tgl_investigasi      =   date('Y-m-d H:i:s');
                    $data->save();

                    echo'ok';

                }
            }else{
                if($request->alasan==''){
                    echo '<ul style="padding: 0px;background:aqua"><li class="lii"><i class="fa fa-times-circle"></i> Isi Alasan terlebih dahulu</li></ul>';
                }else{
                    $data               =   Laporan::find($request->id);
                    $data->alasan          =   $request->alasan;
                    $data->progres      =   $request->progres;
                    $data->tgl_investigasi      =   date('Y-m-d H:i:s');
                    $data->save();

                    echo'ok';
                }
            }
        }
    }

    public function proses_keputusan(request $request){
        if (trim($request->progres) == '') {$error[] = '<li class="lii"><i class="fa fa-times-circle"></i> Pilih Keputusan terlebih dahulu</li>';}
        if (isset($error)) {echo '<ul style="padding: 0px;background:aqua">'.implode('', $error).'</ul>';} 
        else{
            if($request->progres=='T'){  
                $data               =   Laporan::find($request->id);
                $data->progres       =   $request->progres;
                $data->alasan       =   'Tidak Terbukti Bersalah';
                $data->tgl_keputusan      =   date('Y-m-d H:i:s');
                $data->save();

                echo'ok';
            }else{
                $data               =   Laporan::find($request->id);
                $data->progres       =   $request->progres;
                $data->tgl_keputusan      =   date('Y-m-d H:i:s');
                $data->save();

                echo'ok';
            }

            

               
        }
    }

    public function proses_sanksi(request $request){
        if (trim($request->file) == '') {$error[] = '<li class="lii"><i class="fa fa-times-circle"></i> Upload File BAP terlebih dahulu</li>';}
        if (isset($error)) {echo '<ul style="padding: 0px;background:aqua">'.implode('', $error).'</ul>';} 
        else{
                $file=$_FILES['file']['name'];
                $size=$_FILES['file']['size'];
                $asli=$_FILES['file']['tmp_name'];
                $ukuran=getimagesize($_FILES["file"]['tmp_name']);
                $tipe=explode('.',$_FILES['file']['name']);
                $typee=explode('/',$_FILES['file']['type']);
                $filename='BAP-'.date('Ymdhis').'.'.$typee[1];
                $lokasi='_file/';
                if($typee[1]=='pdf' && $size<=1048576){
                    if(move_uploaded_file($asli, $lokasi.$filename)){
                        $data               =   Laporan::find($request->id);
                        $data->progres       =  4;
                        $data->tgl_selesai      =   date('Y-m-d H:i:s');
                        $data->file_bap      =   $filename;
                        $data->save();

                        echo'ok';
                    }
                }else{
                    echo '<ul style="padding: 0px;"><li class="lii"><i class="fa fa-times-circle"></i> File Harus berformat pdf dan maximal size 1mb</li></ul>';
                }
               
        }
    }

    public function cetak(request $request,$id){
        $ide=base64_decode($id);
        error_reporting(0);
        $data=Laporan::where('tiket',$ide)->first();
        if($request->act==1){
            $pdf = PDF::loadView('pdf.index', compact('data'));
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download('TICKET-ID.pdf');
        }
        if($request->act==2){
            $pdf = PDF::loadView('pdf.index', compact('data'));
            $pdf->setPaper('A4', 'landscape');
            $pdf->output();
            return $pdf->stream();
        }
        
    }
}
