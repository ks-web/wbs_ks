<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Laporan extends Model
{
    protected $table = 'laporan';
    public $timestamps = false;

    public function pegawai()
    {
        return $this->belongsTo('App\Pegawai', 'nik', 'empnik');

    }
    public function investigator()
    {
        return $this->belongsTo('App\Investigator', 'investigator_id', 'id');

    }
}
