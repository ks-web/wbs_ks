@extends('layouts.app_web')
<style>
    td{vertical-align:top;font-size:12px;text-align:justify}
</style>
@section('content')
    <div class="bodynya">
        <div class="cmd-4">
            <div class="isi-1">
                <div class="spngambar-kiri"><img class="img-3" src="{{url('img/megaphone.png')}}"></div>
                <div class="spngambar-kanan"><br>Whistleblowing System / Sistem Pelaporan Pelanggaran</div>
                <div class="spngambar-tengah">
                    <br><b style="display: contents;"><p>Whistleblowing System </b> adalah Suatu sistem yang mengelola pengaduan dan/atau pengungkapan mengenai indikasi tindakan pelanggaran, perilaku melawan hukum, perbuatan yang tidak memenuhi standar etika atau perbuatan lain yang dapat merugikan organisasi maupun pemangku kepentingan (stakeholders) yang dilakukan oleh Karyawan atau Pimpinan Perseroan atau Dewan Komisaris berikut organ pendukungnya serta digunakan untuk mengoptimalkan peran serta insan krakatau steel dan pihak lainnya dalam mengungkapkan pelanggaran yang terjadi di lingkungan Perseroan</p><br>
                    <p><img style="width:15px" src="{{url('img/alert.png')}}"> &nbsp;<b>Identitas Pelapor akan dilindungi dan dirahasiakan oleh Perusahaan</b></p>
                </div>
            </div>
        </div>
        <div class="cmd-6">
            <div class="cmd-bawah">
                <ul class="ulnya">
                    <li class="linya">
							<a href="{{url('lapor')}}"><div class="spngambar"><img class="img-2" src="{{url('img/whistle.png')}}"></div><p class="pnya">Melaporkan Indikasi Pelanggaran</p></a>
					  </li>
                    <li class="linya">
							<a href="#" onclick="pantau()"><div class="spngambar"><img class="img-2" src="{{url('img/tablet.png')}}"></div><p class="pnya">Monitoring Pelaporan</p></a>
					  </li>
					  <li class="linya">
                            <a href="{{url('drop')}}"><div class="spngambar"><img class="img-2" src="{{url('img/mail.png')}}"></div><p class="pnya">Drop Box di Area Kantor dan Pabrik</p></a>
					  </li>
                    <li class="linya">
                            <a href="{{url('pos')}}"><div class="spngambar"><img class="img-2" src="{{url('img/post-office.png')}}"></div><p class="pnya">PT. Pos: PO.Box 007 Cilegon</p></a>
					  </li>
					<li class="linya"><a href="{{url('sms')}}"><div class="spngambar"><img class="img-2" src="{{url('img/smartphone.png')}}"></div><p class="pnya">Whatsapp: +6281119605106</p></a></li>
					<li class="linya"><a href="{{url('email')}}"><div class="spngambar"><img class="img-2" src="{{url('img/email.png')}}"></div><p class="pnya">Email: wbs@krakatausteel.com</p></a></li>
                    
                </ul>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalcari" style="display: none;">
        <div class="modal-dialog" >
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title"><center><b>Monitoring Laporan Anda</b></center></h4>
              </div>
              <div class="modal-body" >
                    <input type="text" class="form-control" id="tiket" placeholder="Masukan Nomor Tiket ID " >
                    <img src="{{url('img/monitor.png')}}" width="30%" style="margin-left:35%">
              </div>
              <div class="modal-footer">
                    <button type="submit" class="btn btn-success" onclick="cari()"style="width:48%">PANTAU</button>
                    <button type="submit" class="btn btn-warning" onclick="batal()" style="width:48%">BATAL</button>
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modalnot" style="display: none;">
        <div class="modal-dialog" >
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">NOTIFIKASI</h4>
              </div>
              <div class="modal-body" style="text-align:center">
                    <img src="{{url('img/warning.png')}}" width="20%">
                    <h3>MASUKAN ID TICKET ANDA</h3>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>
@endsection

@push('ajax')
    <script>
        function pantau(){
            $('#modalcari').modal({backdrop: 'static', keyboard: false});
        }

        function cari(){
            var tiket=$('#tiket').val();
            if(tiket==''){
                $('#modalnot').modal({backdrop: 'static', keyboard: false});
                
            }else{
                window.location.assign("{{url('pantau')}}?ID="+tiket);
            }
            
        }
        
        function batal(){
            location.reload();
        }
        
    </script>
@endpush