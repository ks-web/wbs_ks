<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>WHISTLEBLOWING SYSTEM</title>
        <link rel="shortcut icon" href="https://sso.krakatausteel.com/public/fav.png" >
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{url('/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('/bower_components/font-awesome/css/font-awesome.min.css')}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{url('/bower_components/Ionicons/css/ionicons.min.css')}}">
        <!-- DataTables -->
        <link rel="stylesheet" href="{{url('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
        <!-- Theme style -->
        
        <link rel="stylesheet" href="{{url('/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
        <!-- bootstrap datepicker -->
        <link rel="stylesheet" href="{{url('/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="{{url('/plugins/iCheck/all.css')}}">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="{{url('/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="{{url('/plugins/timepicker/bootstrap-timepicker.min.css')}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{url('/bower_components/select2/dist/css/select2.min.css')}}">
        <link rel="stylesheet" href="{{url('/dist/css/AdminLTE.min.css')}}">
        
        <link rel="stylesheet" href="{{url('/dist/css/skins/_all-skins.min.css')}}">
        <style>
            .headnya{
                width:100%;
                border:solid 1px #070744;
                padding:0.5%;
                display:flex;
                background:#070744;
            }
            
            .footnya {
                position: relative;
                padding:2px 10px 2px 0px;
                color:#fff;
                vertical-align:bottom;
                text-align:right;
                background: rgb(14 13 13 / 50%);
                
            }
            
            
            
            @media only screen and (max-width: 700px) {
                .bodynya{
                    width:100%;
                    border:solid 1px #070744;
                    padding:0.5%;
                    display:content;
                    min-height:400px;
                }
                .img-1{
                    width:100%;
                    margin:0px;
                }
                .img-2{
                    width:100%;
                }
				.img-sumi{
                    width:20%;
						
                }
                .img-3{
                    width:100%;
                    -ms-transform: rotate(-20deg); /* IE 9 */
                    transform: rotate(-20deg);
                }
                .col-1{
                    width:10%;
                    float:left;
                    border:solid 1px #070744;
                    padding:2%;
                }
                .col-2{
                    width:25%;
                    float:left;
                    border:solid 1px #070744;
                    
                }
                .col-6{
                    width:50%;
                    float:left;
                    border:solid 1px #070744;
                    padding: 3% 0% 0% 0%;
                    text-align:center;
                }
                .col-8{
                    width:80%;
                    float:left;
                    border:solid 1px #070744;
                    padding:2%;
                }
                .cmd-4{
                    width:90%;
                    margin:5%;
                   
                    background: rgb(14 13 13 / 50%);
                    padding:0.5%;
                    min-height:100px;
                    display:flex;
                }
                .cmd-6{
                    width:90%;
                    margin:5%;
                   
                    background: rgb(14 13 13 / 50%);
                    padding:1%;
                    min-height:100px;
                    display:flow-root;
                }
                .h3-1{
                    color:#fff;
                    font-size:18px;
                    font-weight:bold;
                    margin:0px;
                }
                .h1-1{
                    color:#fff;
                    font-size:14px;
                    font-weight:bold;
                    margin:0px;
                }
                .spngambar{
                    width:50px;
                    height:50px;
                    display: inline-block;
                }
                .spngambar-2{
                    width:30px;
                    height:30px;
                    display: inline-block;
                }
                .linya{
                    list-style:none;
                    width:100%;
                    display: flex;
                    color:#fff;
                    margin-bottom:1%;
                    font-weight:bold;
                    vertical-align:middle;
                    background:none;

                }
                .spngambar-kiri{
                    width:50%;
                    height:90px;
                    padding:5%;
                    display: flex;
                    float:left;
                    
                    
                }
                .spngambar-kanan{
                    width:50%;
                    height:90px;
                    display: flex;
                    float:left;
                    
                    vertical-align:middle;
                    line-height: 20px;
                    text-align: left;
                    font-size:12px;
                    font-weight:bold;
                }
                .cmd-bawah{
                    width: 100%;
                   display:block;
                }
                .cmd-bawah-2{
                    width: 100%;
                   display:block;
                   margin-top:4%;
                }
                .pnya{
                    display:inline;
                    margin-top: 16px;
                    margin-left:5px;
                    font-size:15px;
                }
                .pnya-2{
                    display:inline;
                    margin-top: 13px;
                    margin-left:5px;
                    font-size:13px;
                }
                .bodymodal{
                    height:450px;overflow-x:scroll;
                }
                .isibody{
                    width: 92%;
                    margin:4%;
                }
                .ulnya{
                   width: 100%;
                   padding: 1%;
                }
                .hr-1{
                    border-bottom:solid 3px #fff;
                    margin:0px 0px 20px 0px;
                    width:160px;
                }
                th{
                    font-size:11px;
                }
                td{
                    font-size:11px;
                }
                .progresnya{
                    width: 100%;
                    /* border: solid 2px #ada4a4; */
                    padding-top: 2%;
                    padding-bottom: 2%;
                    display: flex;
                }
                .col-progres{
                    float:left;
                    
                    margin:0.5%;
                    width:18.8%;
                    text-align:center;
                    font-size: 7px;
                    font-weight: bold;
                    height:30px;
                    background-image:url({{url('img/proses.png')}});
                    background-repeat: no-repeat;
                    background-size:100%;
                }
                .col-selesai{
                    float:left;
                    
                    margin:0.5%;
                    width:18.8%;
                    text-align:center;
                    font-size: 7px;
                    font-weight: bold;
                    height:30px;
                    color:#fff;
                    background-image:url({{url('img/selesai.png')}});
                    background-repeat: no-repeat;
                    background-size:100%;
                }
                .tabelcenter{
                    background-color: white;text-align: center;width:100%;
                }
                .imgcenter{
                    width:50%
                }
                .tdcenter{
                    padding:3%;text-align:center;
                }
                
            }
            @media only screen and (min-width: 768px) {
                .bodynya{
                    width:100%;
                    border:solid 1px #070744;
                    padding:0.5%;
                    display:flex;
                    min-height:400px;
                }
                .img-1{
                    width:70%;
                    margin:0px;
                }
                .img-2{
                    width:100%;
                }
                .img-3{
                    width:100%;
                    -ms-transform: rotate(-20deg); /* IE 9 */
                    transform: rotate(-20deg);
                }
                .col-1{
                    width:10%;
                    float:left;
                    border:solid 1px #070744;
                    padding:2%;
                }
                .col-2{
                    width:20%;
                    float:left;
                    border:solid 1px #070744;
                    
                }
                .col-6{
                    width:60%;
                    float:left;
                    border:solid 1px #070744;
                    padding: 3% 0% 0% 0%;
                    text-align:center;
                }
                .col-8{
                    width:80%;
                    float:left;
                    border:solid 1px #070744;
                    padding:2%;
                }
                .h3-1{
                    color:#fff;
                    font-size:30px;
                    font-weight:bold;
                    margin:0px;
                }
                .h1-1{
                    color:#fff;
                    font-size:17px;
                    font-weight:bold;
                    margin-left:40px;
                }
                .cmd-4{
                    width:29%;
                    margin:1%;
                    float:left;
                    background: rgb(14 13 13 / 50%);
                    padding:0.2%;
                    min-height:100px;
                    margin-left:8%;
                    display:flex;
                }
                .cmd-6{
                    width:60%;
                    margin:1%;
                    float:left;
                    background: rgb(14 13 13 / 50%);
                    padding: 2% 2% 2% 1%;
                    min-height:100px;
                    display:initial;
                }
                .spngambar{
                    width:60px;
                    height:60px;
                    display: inline-block;
                }
                .spngambar-kiri{
                    width:40%;
                    height:130px;
                    padding:5%;
                    display: flex;
                    float:left;
                    
                    
                }
                .bodymodal{
                    height:400px;overflow-x:scroll;
                }
                .spngambar-kanan{
                    width:50%;
                    height:130px;
                    display: flex;
                    float:left;
                    
                    vertical-align:middle;
                    line-height: 20px;
                    text-align: left;
                    font-size:12px;
                    font-weight:bold;
                }
                .spngambar-tengah{
                    width:100%;
                    min-height:130px;
                    display: initial;
                    float:left;
                    padding:1%;
                    vertical-align:middle;
                    line-height: 20px;
                    text-align: justify;
                    font-size:12px;
                    
                }
                .spngambar-2{
                    width:40px;
                    height:40px;
                    display: inline-block;
                }
                .linya{
                    list-style:none;
                    width:100%;
                    display: flex;
                    color:#fff;
                    margin-bottom:1%;
                    font-weight:bold;
                    vertical-align:middle;

                }
                .pnya{
                    display:inline;
                    margin-top: 30px;
                    margin-left:10px;
                    font-size:17px;
                }
                .pnya-2{
                    display:inline;
                    margin-top: 30px;
                    margin-left:10px;
                    font-size:14px;
                }
                .ulnya{
                   width: 90%;
                   display:block;
                }
                .cmd-bawah{
                    width: 100%;
                   display:block;
                }
                .cmd-bawah-2{
                    width: 100%;
                   display:block;
                   margin-top:4%;
                }
                .isibody{
                    width: 60%;
                    margin-left:20%;
                }
                .hr-1{
                    border-bottom:solid 3px #fff;
                    margin:0px 0px 20px 40px;
                    width:160px;
                }
                .progresnya{
                    width: 100%;
                    /* border: solid 2px #ada4a4; */
                    padding-top: 2%;
                    padding-bottom: 2%;
                    display: flex;
                }
                .col-progres{
                    float:left;
                    
                    margin:0.5%;
                    width:18.8%;
                    text-align:center;
                    font-size: 12px;
                    font-weight: bold;
                    height:71px;
                    background-image:url({{url('img/proses.png')}});
                    background-repeat: no-repeat;
                    background-size:100%;
                }
                .col-selesai{
                    float:left;
                    
                    margin:0.5%;
                    width:18.8%;
                    text-align:center;
                    font-size: 12px;
                    font-weight: bold;
                    height:71px;
                    color:#fff;
                    background-image:url({{url('img/selesai.png')}});
                    background-repeat: no-repeat;
                    background-size:100%;
                }
                .tabelcenter{
                    background-color: white;text-align: center;width:60%;margin-left:20%;
                }
                .imgcenter{
                    width:50%
                }
                .tdcenter{
                    padding:3%;text-align:center;
                }
            
            }
            .linya:hover{
                background: rgb(99 94 94 / 50%);
            }  
            .lii{
                list-style:none;
                background:#e8e8f1;
                font-size:13px;
                width:100%;
                padding:1%;
                margin: 0.2%;
                font-style:italic;
                
            } 
            a{
                color:#fff;
            } 
            .isi-1{
                width:100%;
                margin:1%;
                min-height:100px;
                background:#fff;
                padding: 3%;
            }
            
        </style>
    </head>
    <body style="background-image:url({{url('img/back.png')}})">
        <div class="headnya">
            <div class="col-2">
                <img class="img-1" src="{{url('img/bumn.png')}}">
            </div>
            <div class="col-6">
                <h3 class="h3-1">WHISTLEBLOWING SYSTEM</h3>
            </div>
            <div class="col-2" style="text-align:right">
                <img class="img-1" src="{{url('img/ks.png')}}">
            </div>
        </div>
        @yield('content')
        <div class="footnya">
            &copy; Divisi Legal, Risk & Compliance
        </div>
        <script src="{{url('/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{url('/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script src="{{url('/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
        <!-- DataTables -->
        <script src="{{url('/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{url('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>


        <!-- InputMask -->
        <script src="{{url('/plugins/input-mask/jquery.inputmask.js')}}"></script>
        <script src="{{url('/plugins/input-mask/jquery.inputmask.date.extensions.js')}}"></script>
        <script src="{{url('/plugins/input-mask/jquery.inputmask.extensions.js')}}"></script>
        <!-- date-range-picker -->
        <script src="{{url('/bower_components/moment/min/moment.min.js')}}"></script>
        <script src="{{url('/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        <!-- bootstrap datepicker -->
        <script src="{{url('/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
        <!-- bootstrap color picker -->
        <script src="{{url('/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js')}}"></script>
        <!-- bootstrap time picker -->
        <script src="{{url('/plugins/timepicker/bootstrap-timepicker.min.js')}}"></script>
        <script src="{{url('/plugins/iCheck/icheck.min.js')}}"></script>

        <!-- SlimScroll -->
        <script src="{{url('/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
        <!-- FastClick -->
        <script src="{{url('/bower_components/fastclick/lib/fastclick.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{url('/dist/js/adminlte.min.js')}}"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="{{url('/dist/js/demo.js')}}"></script>
        <!-- page script -->
        @stack('ajax')
        <script>
            var ww = $(window).width();
            var limit = 769;


            function refresh() {
                ww = $(window).width();
                var w =  ww<limit ? (location.reload(true)) :  ( ww>limit ? (location.reload(true)) : ww=limit );
            }

            var tOut;
            $(window).resize(function() {
                var resW = $(window).width();
                clearTimeout(tOut);
                if ( (ww>limit && resW<limit) || (ww<limit && resW>limit) ) {        
                    tOut = setTimeout(refresh, 100);
                }
            });
        </script>
        <script type="text/javascript">
                
            var rupiah = document.getElementById('rupiah');
                rupiah.addEventListener('click', function(e){
                rupiah.value = formatRupiah(this.value, 'Rp. ');
            });

            var rupiah = document.getElementById('rupiah');
                rupiah.addEventListener('keyup', function(e){
                rupiah.value = formatRupiah(this.value, 'Rp. ');
            });

            var rupiah1 = document.getElementById('rupiah1');
                rupiah1.addEventListener('click', function(e){
                rupiah1.value = formatRupiah(this.value, 'Rp. ');
            });

            var rupiah1 = document.getElementById('rupiah1');
                rupiah1.addEventListener('keyup', function(e){
                rupiah1.value = formatRupiah(this.value, 'Rp. ');
            });
           
        </script>
        <script>
        
            $(function () {
                //Initialize Select2 Elements
                $('.select2').select2()
                $('.select3').select2()

                //Datemask dd/mm/yyyy
                $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
                //Datemask2 mm/dd/yyyy
                $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
                //Money Euro
                $('[data-mask]').inputmask()

                //Date range picker
                $('#reservation').daterangepicker()
                //Date range picker with time picker
                $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, locale: { format: 'MM/DD/YYYY hh:mm A' }})
                //Date range as a button
                $('#daterange-btn').daterangepicker(
                {
                    ranges   : {
                    'Today'       : [moment(), moment()],
                    'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month'  : [moment().startOf('month'), moment().endOf('month')],
                    'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate  : moment()
                },
                function (start, end) {
                    $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
                }
                )

                //Date picker
                $('#datepicker').datepicker({
                    format:"yyyy-mm-dd",
                    autoclose: true
                })
                $('#datepicker2').datepicker({
                    format:"yyyy-mm-dd",
                    autoclose: true
                })

                //iCheck for checkbox and radio inputs
                $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                    checkboxClass: 'icheckbox_minimal-blue',
                    radioClass   : 'iradio_minimal-blue'
                })
                //Red color scheme for iCheck
                $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass   : 'iradio_minimal-red'
                })
                //Flat red color scheme for iCheck
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass   : 'iradio_flat-green'
                })

                //Colorpicker
                $('.my-colorpicker1').colorpicker()
                //color picker with addon
                $('.my-colorpicker2').colorpicker()

                //Timepicker
                $('.timepicker').timepicker({
                    showInputs: false
                })
            })
        </script>
    </body>
</html>

