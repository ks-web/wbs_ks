@extends('layouts.app_web')
<style>
    td{vertical-align:top;font-size:12px;text-align:justify}
</style>
@section('content')
    <div class="bodynya">
        <div class="isibody">
            <div class="box box-primary">
                <div class="box-header with-border" style="text-align:center">
                    <h3 class="box-title"><b>PT POS: PO Box 007 Cilegon</b> </h3>
                </div>
                
                
                
                    <div class="box-body">
                        <table width="100%" border="0">
                            <tr>
                                <td  width="20%"  style="padding:2%">
                                    <img class="img-2" src="{{url('img/post-office.png')}}">
                                </td>
                                <td width="50%"   style="padding:2%;font-size:12pt">
                                        Pihak-pihak yang ingin menyampaikan laporan melalui pos dapat menyampaikan laporannya dengan amplop tertutup dilengkapi dengan:
                                        <br>
                                        <br>
                                        <ol>
                                        <li>Nama lengkap sesuai KTP</li>
                                        <li>Alamat lengkap sesuai KTP dan Nomor KTP </li>
                                        <li>Nama yang dilaporkan</li>
                                        <li>Jelaskan  kronologis kejadian yang dilaporkan </li>
                                        <li>Lampirkan data-data pendukung pelaporan</li>
										</ol>
                                        <br>
										Laporan tersebut dapat dikirim dan/atau diantar langsung ke kantor pos terdekat dengan alamat kirim PO.Box 007 Cilegon.

                                </td>
                            </tr>
                        </table>
                    </div>
               
                <!-- /.box-body -->

                    <div class="box-footer" style="text-align:right">
                        <button type="submit" class="btn btn-success" onclick="batal()" style="width:20%">Kembali</button>
                    </div>
                
                
            </div>
        </div>
    </div>



    
@endsection

@push('ajax')
    <script>
        
        function batal(){
            window.location.assign("{{url('/')}}");
        }
        
    </script>
@endpush