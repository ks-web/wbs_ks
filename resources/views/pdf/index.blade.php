<html>
    <head>
        <title>TICKET-ID</title>
        <script>
            
            window.print();
            
        </script>
        <style>
            .isi{
                width:50%;
                margin-left:25%;
                background:#f1e7e7;
                border:solid 1px #bfb7b7;
                
                
               
            }
            .tabelcenter{
                width:100%;
            }
            .imgcenter{
                width:50%;
                margin-left:25%;
            }
            .tdcenter{
                text-align:center;
            }
        </style>
    </head>
    <body>
        <div class="isi">
            
            <table border="0"  class="tabelcenter">
                <tr>
                    <td class="tdcenter"><img src="img/id-card.png" style="width:60%"></td>
                </tr>
                <tr>
                    <td class="tdcenter"><p><b>Nomor Ticket Pelaporan Anda  {{$data['tiket']}} <br><br>MOHON AGAR NOMOR TIKET ID<br>PELAPORAN INI DISIMPAN UNTUK<br>MEMONITOR TINDAK LANJUT<br>PELAPORAN</b></p></td>
                </tr>
                <tr>
                    <td class="tdcenter"><b>Terima Kasih</b><br><br></td>
                </tr>
                <tr>
                    <td style="text-align:right"><i>© Divisi Legal, Risk & Compliance</i></td>
                </tr>
            </table>
        </div>
        
    </body>
</html>