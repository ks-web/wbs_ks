@extends('layouts.app_web')
<style>
    td{vertical-align:top;font-size:12px;text-align:justify}
</style>
@section('content')
    <div class="bodynya">
        <div class="isibody">
            <div class="box box-primary">
                <div class="box-header with-border" style="text-align:center">
                    <h3 class="box-title"><b>Drop Box</b> </h3>
                </div>
                
                
                
                    <div class="box-body">
                        <table width="100%" border="0" >
                            <tr>
                                <td rowspan="3" width="20%"  style="padding:2%">
                                    <img class="img-2" src="{{url('img/mail.png')}}">
                                </td>
                                <td style="font-size:12pt">Perseroan menyediakan kotak pengaduan (Drop Box) yang ditempatkan di beberapa tempat yaitu:<br>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size:12pt"><br><b>Cilegon:</b><br>
										<ol>
											<li>Gedung HCD & LC (Pusdiklat)</li>
											<li>Gedung Teknologi</li>
											<li>Gedung Keamanan</li>
											<li>Gedung Produksi</li>
											<li>Gedung EDP</li>
											<li>Gedung Logistik</li>
											<li>Gedung Perencanaan</li>
											<li>Area HSM</li>
											<li>Area CRM</li>
										</ol>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-size:12pt"><b>Jakarta:</b><br>
										<ol>
											<li>Gedung Krakatau Steel Jakarta</li>
										</ol>
                                </td>
                            </tr>
                        </table>
                    </div>
               
                <!-- /.box-body -->

                    <div class="box-footer" style="text-align:right">
                        <button type="submit" class="btn btn-success" onclick="batal()" style="width:20%">Kembali</button>
                    </div>
                
                
            </div>
        </div>
    </div>



    
@endsection

@push('ajax')
    <script>
        
        function batal(){
            window.location.assign("{{url('/')}}");
        }
        
    </script>
@endpush