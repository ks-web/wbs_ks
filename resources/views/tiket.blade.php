@extends('layouts.app_web')
<style>
    td{vertical-align:top;font-size:12px;text-align:justify}
    #tiket{
        margin-top:10%;
    }
</style>
@section('content')
    <div class="bodynya" >
        <div class="isibody">
            <div  id="tiket">
                <img src="{{url('img/Logo PTKS.png')}}" style="width:50%;margin-left:25%;">
				<br>
				<br>
				<br>
				<br>
					<table border=0  class="tabelcenter">
						<tr>
							<td class="tdcenter"><img src="{{url('img/id-card.png')}}" class="imgcenter"></td>
						</tr>
						<tr>
							<td class="tdcenter"><p><b>Nomor Ticket Pelaporan Anda  {{$data['tiket']}} <br><br>MOHON AGAR NOMOR TIKET ID<br>PELAPORAN INI DISIMPAN UNTUK<br>MEMONITOR TINDAK LANJUT<br>PELAPORAN</b></p></td>
						</tr>
						<tr>
							<td class="tdcenter"><b>Terima Kasih</b><br><br></td>
						</tr>
					</table>
					
                
            </div>
            
			<br>
            <center>
            <a href="{{url('/')}}" ><span class="btn btn-info" style="width:23%"><i class="fa fa-home"></i> Kembali</span></a>
            <a href="{{url('admin/cetak/'.base64_encode($data['tiket']).'?act=1')}}" target="_blank"><span class="btn btn-primary" style="width:23%"><i class="fa fa-cloud-download" ></i> Unduh</span></a>
            <a href="{{url('admin/cetak/'.base64_encode($data['tiket']).'?act=2')}}" target="_blank"><span class="btn btn-warning" style="width:23%"><i class="fa fa-print"></i> Cetak</span></a>
            </center>
        </div>
        
    </div>



    <div class="modal fade" id="modalnik" style="display: none;">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body" >
                    <input type="text" class="form-control" onkeyup="cari(this.value)" placeholde="Ketikan NIK atau Nama">
                    <div  class="bodymodal" id="tampilkan">
                    </div>
                    
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalnot" style="display: none;">
        <div class="modal-dialog" >
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">NOTIFIKASI</h4>
              </div>
              <div class="modal-body" >
                    <div id="notifikasi"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>
@endsection

@push('ajax')
    <script>
        $('#tampil_nama_pelanggar').hide();
        $('#foot2').hide();
        var rad = document.myForm.sts;
        var prev = null;
        for(var i = 0; i < rad.length; i++) {
            rad[i].onclick = function () {
                
                if(this.value==1){
                    $('#tampil_nama_pelanggar').show();
                    $('#modalnik').modal({backdrop: 'static', keyboard: false});
                    $.ajax({
                        type: 'GET',
                        url: "{{url('lapor/view_data')}}",
                        data: "id=id",
                        beforeSend: function(){
                                $("#tampilkan").html('<center><img src="{{url('/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
                        },
                        success: function(msg){
                                $('#modalloading').modal('hide');
                                $("#tampilkan").html(msg);
                            
                        }
                    });
                }else{
                    $('#tampil_nama_pelanggar').hide();
                    $('#namapegawai').val('');
                    $('#nik').val('');
                }
            };
        }

        function cari(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('lapor/view_data')}}?name="+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkan").html(msg);
                  
               }
           });
            
        }
        function batal(){
            location.reload();
        }
        function showpegawai(){
           
            $('#modalnik').modal({backdrop: 'static', keyboard: false});
            $.ajax({
                type: 'GET',
                url: "{{url('lapor/view_data')}}",
                data: "id=id",
                beforeSend: function(){
                        $("#tampilkan").html('<center><img src="{{url('/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
                },
                success: function(msg){
                        $('#modalloading').modal('hide');
                        $("#tampilkan").html(msg);
                    
                }
            });
            
        }

        function pilih(nik,nama){
            $('#namapegawai').val('['+nik+'] '+nama);
            $('#nik').val(nik);
            $('#modalnik').modal('hide');
        }

        function simpan_data(){
            var form=document.getElementById('mysimpan_data');
            
                $.ajax({
                    type: 'POST',
                    url: "{{url('/lapor/simpan')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#foot1').hide();
                        $('#foot2').show();
                    },
                    success: function(msg){
                        data=msg.split('|');
                        if(data[0]=='ok'){
                            window.location.assign("{{url('tiket')}}?NOTIKET="+data[1]+"@"+data[2]);
                               
                        }else{
                            $('#modalnot').modal({backdrop: 'static', keyboard: false});
                            $('#foot1').show();
                            $('#foot2').hide();
                            $('#notifikasi').html(msg);
                        }
                        
                        
                    }
                });

        } 
    </script>
@endpush