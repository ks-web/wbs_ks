@extends('layouts.app_web')
<style>
    td{vertical-align:top;font-size:12px;text-align:justify}
</style>
@section('content')
    <div class="bodynya">
        <div class="isibody">
            <div class="box box-primary">
                <div class="box-header with-border" style="text-align:center">
                    <h3 class="box-title"><b>Email</b> </h3>
                </div>
                
                
                
                    <div class="box-body">
                        <table width="100%" border="0">
                            <tr>
                                <td rowspan="2" width="20%"  style="padding:2%">
                                    <img class="img-2" src="{{url('img/email.png')}}">
                                </td>
                                <td style="font-size:11pt">Pihak-pihak yang berkeinginan menyampaikan laporan melalui email dapat menyampaikan laporannya ke wbs@krakatausteel.com dengan langkah pelaporan sebagai berikut: 
                                     </td>
                            </tr>
                            <tr>
                                <td style="font-size:11pt"><br>Langkah 1 : Tulis nama lengkap sesuai KTP<br>
										Langkah 2 : Tulis alamat lengkap sesuai KTP<br>
										Langkah 3 : Tulis nomor KTP<br>
										Langkah 4 : Tulis  nama terlapor<br>
										Langkah 5 : Jelaskan kronologis kejadian<br>
										Langkah 6 : Lampirkan data-data pendukung pelaporan
                                </td>
                            </tr>
                            
                        </table>
                    </div>
               
                <!-- /.box-body -->

                    <div class="box-footer" style="text-align:right">
                        <button type="submit" class="btn btn-success" onclick="batal()" style="width:20%">Kembali</button>
                    </div>
                
                
            </div>
        </div>
    </div>



    
@endsection

@push('ajax')
    <script>
        
        function batal(){
            window.location.assign("{{url('/')}}");
        }
        
    </script>
@endpush