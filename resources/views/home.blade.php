@extends('layouts.app_admin')
<style>
    th{
        font-size:12px;
        background:#b0e6e6;
        border:solid 1px #d1d1d6;
        padding:5px;
    }
    td{
        font-size:12px;
        border:solid 1px #d1d1d6;
        padding:3px;
    }
    .ttd{
        font-size:12px;
        border:solid 1px #d1d1d6;
        padding:5px;
    }
    
</style>
@section('content')

<section class="content">
    <div class="row">
        <div class="col-lg-4 col-xs-6">
          
            <div class="small-box bg-blue">
                <div class="inner">
                    <h3>{{rekap_laporan('all')}}</h3>

                    <p>TOTAL PELAPORAN</p>
                </div>
                <div class="icon" style="top: -2;">
                    <i class="ion ion-archive"></i>
                </div>
                {{-- <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a> --}}
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>{{rekap_laporan('0')}}</h3>

                    <p>BELUM DIPROSES</p>
                </div>
                <div class="icon" style="top: -2;">
                    <i class="ion ion-document"></i>
                </div>
                <a href="{{url('/admin')}}?progres=0" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          
            <div class="small-box bg-navy">
                <div class="inner">
                    <h3>{{rekap_laporan('1')}}</h3>

                    <p>Investigasi</p>
                </div>
                <div class="icon" style="top: -2;">
                    <i class="ion ion-search"></i>
                </div>
                <a href="{{url('/admin')}}?progres=1" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>{{rekap_laporan('X')}}</h3>

                    <p>TIDAK DAPAT DI PROSES</p>
                </div>
                <div class="icon" style="top: -2;">
                    <i class="ion ion-pie-graph"></i>
                </div>
                <a href="{{url('/admin')}}?progres=X" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>{{rekap_laporan('T')}}</h3>

                    <p>Tidak Terbukti Bersalah</p>
                </div>
                <div class="icon" style="top: -2;">
                    <i class="ion ion-pricetag"></i>
                </div>
                <a href="{{url('/admin')}}?progres=T" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          
            <div class="small-box bg-maroon">
                <div class="inner">
                    <h3>{{rekap_laporan('2')}}</h3>

                    <p>Terbukti Bersalah</p>
                </div>
                <div class="icon" style="top: -2;">
                    <i class="ion ion-bookmark"></i>
                </div>
                <a href="{{url('/admin')}}?progres=2" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        
    </div>
</section>

<div class="modal fade" id="modalproses" style="display: none;">
    <div class="modal-dialog" style="margin-top: 0%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Verifikasi Admin</h4>
            </div>
            <div class="modal-body">
                <div id="notifikasiproses"></div>
                <form method="post" id="myubah_data" enctype="multipart/form-data">
                    @csrf
                    
                    <div id="tampilkanproses" ></div>
                        
                    
                </form>
            </div>
            <div class="modal-footer" id="ubah_data_proses_1">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="ubah_data()">Simpan Data</button>
            </div>
            <div class="modal-footer" id="ubah_data_proses_2">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" >Proses.....</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalkeputusan" style="display: none;">
    <div class="modal-dialog" style="margin-top: 0%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Keputusan Whistleblowing</h4>
            </div>
            <div class="modal-body">
                <div id="notifikasikeputusan"></div>
                <form method="post" id="myubah_data_keputusan" enctype="multipart/form-data">
                    @csrf
                    
                    <div id="tampilkankeputusan" ></div>
                        
                    
                </form>
            </div>
            <div class="modal-footer" id="ubah_data_keputusan_1">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="ubah_data_keputusan()">Simpan Data</button>
            </div>
            <div class="modal-footer" id="ubah_data_keputusan_2">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" >Proses.....</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalsanksi" style="display: none;">
    <div class="modal-dialog" style="margin-top: 0%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Sanksi Whistleblowing</h4>
            </div>
            <div class="modal-body">
                <div id="notifikasisanksi"></div>
                <form method="post" id="myubah_data_sanksi" enctype="multipart/form-data">
                    @csrf
                    
                    <div id="tampilkansanksi" ></div>
                        
                    
                </form>
            </div>
            <div class="modal-footer" id="ubah_data_sanksi_1">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="ubah_data_sanksi()">Simpan Data</button>
            </div>
            <div class="modal-footer" id="ubah_data_sanksi_2">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" >Proses.....</button>
            </div>
        </div>
    </div>
</div>


@endsection

