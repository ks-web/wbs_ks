@extends('layouts.app_admin')
<style>
    th{
        font-size:12px;
        background:#b0e6e6;
        border:solid 1px #d1d1d6;
        padding:5px;
    }
    td{
        font-size:12px;
        border:solid 1px #d1d1d6;
        padding:3px;
    }
    .ttd{
        font-size:12px;
        border:solid 1px #d1d1d6;
        padding:5px;
    }
    
</style>
@section('content')

<section class="content">
    <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header" style="margin-bottom:1%">
              <h3 class="box-title">
                <!-- <span class="btn btn-success btn-sm" onclick="tambah()">Tambah Baru</span> -->
                </h3>

              <div class="box-tools">
                <div class="input-group input-group-sm hidden-xs" style="width: 400px;">
                  <input type="text" name="table_search" onkeyup="cari(this.value)" class="form-control pull-right" placeholder="Search">

                  <div class="input-group-btn">
                    <button type="submit" class="btn btn-default" ><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="@if($progres=='0') active @endif"><a href="#" onclick="cek_page(0)">VERIFIKASI ADMIN</a></li>
                    <li class="@if($progres=='1') active @endif"><a href="#"  onclick="cek_page(1)" aria-expanded="false">PROSES INVESTIGASI</a></li>
                    <li class="@if($progres=='2') active @endif"><a href="#"  onclick="cek_page(2)" aria-expanded="false">TERBUKTI BERSALAH</a></li>
                    <li class="@if($progres=='T') active @endif"><a href="#"  onclick="cek_page('T')" aria-expanded="false">TERBUKTI TIDAK BERSALAH</a></li>
                    <li class="@if($progres=='X') active @endif"><a href="#"  onclick="cek_page('X')" aria-expanded="false">TIDAK BISA DIPROSES</a></li>
                </ul>
            </div>
            <div class="box-body table-responsive no-padding" id="tampilkan" style="padding:100px">
            
                
            
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>
</section>

<div class="modal fade" id="modalproses" style="display: none;">
    <div class="modal-dialog" style="margin-top: 0%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Verifikasi Admin</h4>
            </div>
            <div class="modal-body">
                <div id="notifikasiproses"></div>
                <form method="post" id="myubah_data" enctype="multipart/form-data">
                    @csrf
                    
                    <div id="tampilkanproses" ></div>
                        
                    
                </form>
            </div>
            <div class="modal-footer" id="ubah_data_proses_1">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="ubah_data()">Simpan Data</button>
            </div>
            <div class="modal-footer" id="ubah_data_proses_2">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" >Proses.....</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalkeputusan" style="display: none;">
    <div class="modal-dialog" style="margin-top: 0%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Keputusan Whistleblowing</h4>
            </div>
            <div class="modal-body">
                <div id="notifikasikeputusan"></div>
                <form method="post" id="myubah_data_keputusan" enctype="multipart/form-data">
                    @csrf
                    
                    <div id="tampilkankeputusan" ></div>
                        
                    
                </form>
            </div>
            <div class="modal-footer" id="ubah_data_keputusan_1">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="ubah_data_keputusan()">Simpan Data</button>
            </div>
            <div class="modal-footer" id="ubah_data_keputusan_2">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" >Proses.....</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalsanksi" style="display: none;">
    <div class="modal-dialog" style="margin-top: 0%;">
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Sanksi Whistleblowing</h4>
            </div>
            <div class="modal-body">
                <div id="notifikasisanksi"></div>
                <form method="post" id="myubah_data_sanksi" enctype="multipart/form-data">
                    @csrf
                    
                    <div id="tampilkansanksi" ></div>
                        
                    
                </form>
            </div>
            <div class="modal-footer" id="ubah_data_sanksi_1">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" onclick="ubah_data_sanksi()">Simpan Data</button>
            </div>
            <div class="modal-footer" id="ubah_data_sanksi_2">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary" >Proses.....</button>
            </div>
        </div>
    </div>
</div>


@endsection

@push('datatable')
    <script>
        function tambah(){
            $('#modal-default').modal({backdrop: 'static', keyboard: false});
        }
        function importdata(){
            $('#modal-import').modal({backdrop: 'static', keyboard: false});
        }
        
        $('#ubah_data_proses_2').hide();
        $('#ubah_data_keputusan_2').hide();
        $('#ubah_data_sanksi_2').hide();
       
        $(document).ready(function() {
            

            $.ajax({
               type: 'GET',
               url: "{{url('admin/view_data?progres='.$progres)}}",
               data: "id=id",
               beforeSend: function(){
                    $("#tampilkan").html('<center><img src="{{url('/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
               },
               success: function(msg){
                    $('#modalloading').modal('hide');
                    $("#tampilkan").html(msg);
                  
               }
           });
            

        });

        function cek_page(a){
           
            window.location.assign("{{url('admin')}}?progres="+a);
            
        }
        function pilih_persetujuan(a){
           
            if(a==''){
                $('#alasan').hide();
                $('#pic').hide();
            }
            if(a=='1'){
                $('#alasan').hide();
                $('#pic').show();
            }
            if(a=='X'){
                $('#alasan').show();
                $('#pic').hide();
            }
            
        }

        function cari(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('admin/view_data?progres='.$progres)}}&name="+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkan").html(msg);
                  
               }
           });
            
        }

        function proses(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('admin/proses')}}/"+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkanproses").html(msg);
                   $('#modalproses').modal({backdrop: 'static', keyboard: false});
                  
               }
           });
            
        }

        function keputusan(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('admin/keputusan')}}/"+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkankeputusan").html(msg);
                   $('#modalkeputusan').modal({backdrop: 'static', keyboard: false});
                  
               }
           });
            
        }

        function sanksi(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('admin/sanksi')}}/"+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkansanksi").html(msg);
                   $('#modalsanksi').modal({backdrop: 'static', keyboard: false});
                  
               }
           });
            
        }

        function cari_unit(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('kpi/cari_unit')}}?name="+a,
               data: "id=id",
               success: function(msg){
                   var data=msg.split('/');
                   $("#nama_unit").val('['+data[1]+']'+data[0]);
                   $("#unit_id").val(data[1]);
                  
               }
           });
            
        }
        function cek(a){
           alert(a);
           $.ajax({
               type: 'GET',
               url: "{{url('periode/cek')}}/"+a,
               data: "id=id",
               success: function(msg){
                    $('#modalloading').modal('hide');
                    $("#tampilkan").load("{{url('periode/view_data')}}");
                  
               }
           });
            
        }
        function uncek(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('kasir/uncek')}}/"+a,
               data: "id=id",
               success: function(msg){
                    $('#modalloading').modal('hide');
                    $("#tampilkan").load("{{url('kasir/view_data')}}");
                  
               }
           });
            
        }
        

        function hapus(a){
            if (confirm('Apakah yakin akan menghapus data ini?')) {
                $.ajax({
                    type: 'GET',
                    url: "{{url('periode/hapus')}}/"+a,
                    data: "id=id",
                    beforeSend: function(){
                                $('#modalloading').modal({backdrop: 'static', keyboard: false});
                        },
                    success: function(msg){
                            $('#modalloading').modal('hide');
                            $("#tampilkan").load("{{url('periode/view_data')}}");
                        
                    }
                });
            }

        }
        
        

        function cetak(a){
           
            $("#printableArea").load("{{url('kasir/cetak')}}/"+a);
            $('#modalcetak').modal({backdrop: 'static', keyboard: false});
            
        }

        function simpan_data(){
            var form=document.getElementById('mysimpan_data');
            
                $.ajax({
                    type: 'POST',
                    url: "{{url('/periode/simpan')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#modalloading').modal({backdrop: 'static', keyboard: false});
                    },
                    success: function(msg){
                       
                        if(msg=='ok'){
                            $('#modal-default').modal('hide');
                            $('#modalloading').modal('hide');
                            $("#tampilkan").load("{{url('periode/view_data')}}");
                               
                        }else{
                            $('#modalloading').modal('hide');
                            $('#simpan_data').show();
                            $('#notifikasi').html(msg);
                        }
                        
                        
                    }
                });

        } 

        function ubah_data(){
            var form=document.getElementById('myubah_data');
                var id=$('#id').val();
                $.ajax({
                    type: 'POST',
                    url: "{{url('/admin/proses_investigasi')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#ubah_data_proses_1').hide();
                        $('#ubah_data_proses_2').show();
                    },
                    success: function(msg){
                        
                        if(msg=='ok'){
                            location.reload();
                        }else{
                            $('#ubah_data_proses_2').hide();
                            $('#ubah_data_proses_1').show();
                            $('#notifikasiproses').html(msg);
                        }
                        
                        
                    }
                });

        } 
        function ubah_data_keputusan(){
            var form=document.getElementById('myubah_data_keputusan');
                var id=$('#id').val();
                $.ajax({
                    type: 'POST',
                    url: "{{url('/admin/proses_keputusan')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#ubah_data_keputusan_1').hide();
                        $('#ubah_data_keputusan_2').show();
                    },
                    success: function(msg){
                        
                        if(msg=='ok'){
                            location.reload();
                        }else{
                            $('#ubah_data_keputusan_2').hide();
                            $('#ubah_data_keputusan_1').show();
                            $('#notifikasikeputusan').html(msg);
                        }
                        
                        
                    }
                });

        } 
        function ubah_data_sanksi(){
            var form=document.getElementById('myubah_data_sanksi');
                var id=$('#id').val();
                $.ajax({
                    type: 'POST',
                    url: "{{url('/admin/proses_sanksi')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#ubah_data_sanksi_1').hide();
                        $('#ubah_data_sanksi_2').show();
                    },
                    success: function(msg){
                        
                        if(msg=='ok'){
                            location.reload();
                        }else{
                            $('#ubah_data_sanksi_2').hide();
                            $('#ubah_data_sanksi_1').show();
                            $('#notifikasisanksi').html(msg);
                        }
                        
                        
                    }
                });

        } 

        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>
    
@endpush