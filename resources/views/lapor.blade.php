@extends('layouts.app_web')
<style>
    td{vertical-align:top;font-size:12px;text-align:justify}
</style>
@section('content')
    <div class="bodynya">
        <div class="isibody">
            <div class="box box-primary">
                <div class="box-header with-border" style="text-align:center">
                    <h3 class="box-title">FORMULIR LAPORAN </h3>
                </div>
                
                
                <form method="post" name="myForm" id="mysimpan_data" enctype="multipart/form-data">
                    @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Langkah 1. Isi Identitas Diri (<i>* Identitas pelapor akan dilindungi dan dirahasiakan</i>)</label>
                            <input type="text" class="form-control"  name="name" placeholder="Nama Lengkap sesuai KTP dan No. KTP">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Langkah 2. Isi Alamat (wajib diisi)</label>
                            <textarea class="form-control"  name="alamat"  placeholder="Alamat sesuai KTP" rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Langkah 3. Isi Kriteria Dugaan Pelaporan</label>
                            <!-- <input type="text" class="form-control" name="dugaan" placeholder="Enter ........"> -->
                            <select class="form-control"  name="dugaan_1" onchange="cek_dugaan(this.value)" id="dugaan_1">
                                <option value="">Pilih Kriteria</option>
										@foreach(kategori() as $kri){
											<option value="{{ $kri->desc }}">{{ $kri->desc }}</option>
										}
										@endforeach
                            </select>
                            <textarea class="form-control" style="margin-top:1%" name="dugaan_2" onkeyup="isi_dugaan(this.value)" id="dugaan_2" placeholder="Dugaan  ........" rows="4"></textarea>
                            <input type="hidden" class="form-control"  name="dugaan" id="dugaan" placeholder="Nama...">
                        </div>
                        
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1">Langkah 4. Isi Kronologis Pelaporan</label>
                            <textarea class="form-control" name="deskripsi"  placeholder="Ceritakan tentang detail kejadian disini" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Identitas Terlapor</label><br>
                            <input type="radio"   id="radio1" name="sts"   value="1"  > Diketahui &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input type="radio"  id="radio2" name="sts" value="0"  > Tidak Diketahui 
                        </div>
                        <div class="form-group" id="tampil_nama_pelanggar">
                            <label for="exampleInputEmail1">Pelanggar</label><br>
                            <span class="btn btn-success" style="margin-top: -4px;border-radius: 0px;height: 34px;" onclick="showpegawai()"><i class="fa fa-search"></i></span><input type="text" style="width:80%;display:inline" class="form-control"   id="namapegawai" placeholder="Enter ........">
                        </div>
                        
                        <div class="form-group" >
                            <label for="exampleInputFile">Lampiran Dokumen Pendukung Laporan</label>
                            <input type="file" name="file" id="exampleInputFile">                           
                        </div>
                        <input type="hidden" name="nik" id="nik">
                    </div>
                </form>
                <!-- /.box-body -->

                <div class="box-footer" id="foot1">
                    <button type="submit" class="btn btn-primary" onclick="simpan_data()"style="width:49%">KIRIM</button>
                    <button type="submit" class="btn btn-warning" onclick="batal()" style="width:49%">BATALKAN</button>
                </div>
                <div class="box-footer" id="foot2">
                    <button type="submit" class="btn btn-primary" style="width:49%">Proses........</button>
                    <button type="submit" class="btn btn-warning" style="width:49%">BATALKAN</button>
                </div>
                
            </div>
        </div>
    </div>



    <div class="modal fade" id="modalnik" style="display: none;">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body" >
                    <input type="text" class="form-control" onkeyup="cari(this.value)" placeholde="Ketikan NIK atau Nama">
                    <div  class="bodymodal" id="tampilkan">
                    </div>
                    
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalnot" style="display: none;">
        <div class="modal-dialog" >
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">NOTIFIKASI</h4>
              </div>
              <div class="modal-body" >
                    <div id="notifikasi"></div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>
@endsection

@push('ajax')
    <script>
        $('#tampil_nama_pelanggar').hide();
        $('#foot2').hide();
        $('#dugaan_2').hide();
        $('#dugaan_2').val('');
        $('#dugaan').val('');
        var rad = document.myForm.sts;
        var prev = null;
        for(var i = 0; i < rad.length; i++) {
            rad[i].onclick = function () {
                
                if(this.value==1){
                    $('#tampil_nama_pelanggar').show();
                    $('#modalnik').modal({backdrop: 'static', keyboard: false});
                    $.ajax({
                        type: 'GET',
                        url: "{{url('lapor/view_data')}}",
                        data: "id=id",
                        beforeSend: function(){
                                $("#tampilkan").html('<center><img src="{{url('/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
                        },
                        success: function(msg){
                                $('#modalloading').modal('hide');
                                $("#tampilkan").html(msg);
                            
                        }
                    });
                }else{
                    $('#tampil_nama_pelanggar').hide();
                    $('#namapegawai').val('');
                    $('#nik').val('');
                }
            };
        }

        function cari(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('lapor/view_data')}}?name="+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkan").html(msg);
                  
               }
           });
            
        }
        function batal(){
            window.location.assign("{{url('/')}}");
        }
        function showpegawai(){
           
            $('#modalnik').modal({backdrop: 'static', keyboard: false});
            $.ajax({
                type: 'GET',
                url: "{{url('lapor/view_data')}}",
                data: "id=id",
                beforeSend: function(){
                        $("#tampilkan").html('<center><img src="{{url('/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
                },
                success: function(msg){
                        $('#modalloading').modal('hide');
                        $("#tampilkan").html(msg);
                    
                }
            });
            
        }

        function pilih(nik,nama){
            $('#namapegawai').val(nama);
            $('#nik').val(nik);
            $('#modalnik').modal('hide');
        }

        function cek_dugaan(a){
            if(a=='Lainnya'){
                $('#dugaan_2').show();
                $('#dugaan').val(a);
            }else{
                $('#dugaan_2').hide();
                $('#dugaan_2').val('');
                $('#dugaan').val(a);
            }
           
        }

        function isi_dugaan(a){
            $('#dugaan').val(a);
        }

        function simpan_data(){
            var form=document.getElementById('mysimpan_data');
            
                $.ajax({
                    type: 'POST',
                    url: "{{url('/lapor/simpan')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#foot1').hide();
                        $('#foot2').show();
                    },
                    success: function(msg){
                        data=msg.split('|');
                        if(data[0]=='ok'){
                            window.location.assign("{{url('tiket')}}?NOTIKET="+data[1]+"@"+data[2]);
                               
                        }else{
                            $('#modalnot').modal({backdrop: 'static', keyboard: false});
                            $('#foot1').show();
                            $('#foot2').hide();
                            $('#notifikasi').html(msg);
                        }
                        
                        
                    }
                });

        } 
    </script>
@endpush