@extends('layouts.app_web')
<style>
    td{vertical-align:top;font-size:12px;text-align:justify}
    .btn_home {
  background-color: DodgerBlue;
  border: none;
  color: white;
  padding: 12px 16px;
  font-size: 16px;
  cursor: pointer;
}

/* Darker background on mouse-over */
.btn_home:hover {
  background-color: RoyalBlue;
}
</style>
@section('content')
    <div class="bodynya">
        <div class="isibody">
            <div class="box box-primary" style="padding-left:20px;padding-right:20px">
                <div class="box-header with-border" style="text-align:center">
                    <input type="text" class="form-control" id="tiket" placeholder="Masukan Nomor Tiket ID" style="width:78%;display:inline">
                    <span class="btn btn-success" onclick="cari_tiket()"><i class="fa fa-search"></i> Cari</span>
                </div>
                <div class="box-header with-border" style="text-align:center">
                    <h3 class="box-title">PROGRES PELAPORAN ANDA</h3>
                </div>
                @if ($data['tiket'] =='')
                <div class="box-body" style="padding-left:20px;padding-right:20px">
                <p>Data Tidak ADA , silahkan check kembali ID Tiket nya</p>
                </div>
                @else
                <div>
                     <label><b>Nomor Tiket :  </b></label><b> {{$data['tiket']}}</b><br>
                </div>
                <div class="box-body" style="padding-left:20px;padding-right:20px;border: solid 2px #ada4a4;">
                    
                    <label>Pelapor : </label><br>
                    {{$data['name']}}<br>
                    <label>Kategori Pelaporan :</label><br>
                    {{$data['dugaan']}}<br>
					
                    <label>Nama Terlapor</label><br>
                    {{$data->pegawai['empname']}}<br>
                    <label>Status</label><br>
                    <div class="progresnya">
                          <div class="col-selesai"><br>Laporan<br>Terkirim</div> 
                          @if($data['progres']>0) 
                          <div class="col-selesai"><br>Verifikasi<br>Admin</div>  
                          @else
                          <div class="col-progres"><br>Verifikasi<br>Admin</div>  
                          @endif

                          @if($data['progres']>1) 
                          <div class="col-selesai"><br>Dalam Proses<br>Investigasi<br></div>   
                          @else
                          <div class="col-progres"><br>Dalam Proses<br>Investigasi<br></div>  
                          @endif

                          @if($data['progres']>2) 
                          <div class="col-selesai"><br>Pengenaan<br>Sanksi</div>  
                          @else
                          <div class="col-progres"><br>Pengenaan<br>Sanksi</div> 
                          @endif

                          @if($data['progres']>3) 
                          <div class="col-selesai"><br>Selesai</div>    
                          @else
                          <div class="col-progres"><br>Selesai</div>   
                          @endif
                          
                          
                    </div>
                    <label>Catatan</label><br>
                    {{$data['alasan']}}<br>
                </div>
                @endif
                <div class="box-footer" >
                    &nbsp;
                    <a href="{{url('/')}}" ><span class="btn btn-info"><i class="fa fa-home"></i> Kembali</span></a>
                </div>
                
                
            </div>
        </div>
    </div>



    <div class="modal fade" id="modalnik" style="display: none;">
        <div class="modal-dialog modal-lg" >
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Default Modal</h4>
              </div>
              <div class="modal-body" >
                    <input type="text" class="form-control" onkeyup="cari(this.value)" placeholde="Ketikan NIK atau Nama">
                    <div  class="bodymodal" id="tampilkan">
                    </div>
                    
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>
    <div class="modal fade" id="modalnot" style="display: none;">
        <div class="modal-dialog" >
            <div class="modal-content" >
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">NOTIFIKASI</h4>
              </div>
              <div class="modal-body" style="text-align:center">
                    <img src="{{url('img/warning.png')}}" width="20%">
                    <h3>MASUKAN ID TICKET ANDA</h3>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
                
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
          <!-- /.modal-dialog -->
    </div>
@endsection

@push('ajax')
    <script>
        $('#tampil_nama_pelanggar').hide();
        $('#foot2').hide();
        var rad = document.myForm.sts;
        var prev = null;
        for(var i = 0; i < rad.length; i++) {
            rad[i].onclick = function () {
                
                if(this.value==1){
                    $('#tampil_nama_pelanggar').show();
                    $('#modalnik').modal({backdrop: 'static', keyboard: false});
                    $.ajax({
                        type: 'GET',
                        url: "{{url('lapor/view_data')}}",
                        data: "id=id",
                        beforeSend: function(){
                                $("#tampilkan").html('<center><img src="{{url('/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
                        },
                        success: function(msg){
                                $('#modalloading').modal('hide');
                                $("#tampilkan").html(msg);
                            
                        }
                    });
                }else{
                    $('#tampil_nama_pelanggar').hide();
                    $('#namapegawai').val('');
                    $('#nik').val('');
                }
            };
        }

        function cari_tiket(){
            var tiket=$('#tiket').val();
            if(tiket==''){
                $('#modalnot').modal({backdrop: 'static', keyboard: false});
                
            }else{
                window.location.assign("{{url('pantau')}}?ID="+tiket);
            }
            
        }

        function cari(a){
           
           $.ajax({
               type: 'GET',
               url: "{{url('lapor/view_data')}}?name="+a,
               data: "id=id",
               success: function(msg){
                   $("#tampilkan").html(msg);
                  
               }
           });
            
        }
        function batal(){
            location.reload();
        }
        function showpegawai(){
           
            $('#modalnik').modal({backdrop: 'static', keyboard: false});
            $.ajax({
                type: 'GET',
                url: "{{url('lapor/view_data')}}",
                data: "id=id",
                beforeSend: function(){
                        $("#tampilkan").html('<center><img src="{{url('/img/loading.gif')}}" width="3%"> Proses Data.............</center>');
                },
                success: function(msg){
                        $('#modalloading').modal('hide');
                        $("#tampilkan").html(msg);
                    
                }
            });
            
        }

        function pilih(nik,nama){
            $('#namapegawai').val('['+nik+'] '+nama);
            $('#nik').val(nik);
            $('#modalnik').modal('hide');
        }

        function simpan_data(){
            var form=document.getElementById('mysimpan_data');
            
                $.ajax({
                    type: 'POST',
                    url: "{{url('/lapor/simpan')}}",
                    data: new FormData(form),
                    contentType: false,
                    cache: false,
                    processData:false,
                    beforeSend: function(){
                        $('#foot1').hide();
                        $('#foot2').show();
                    },
                    success: function(msg){
                        data=msg.split('|');
                        if(data[0]=='ok'){
                            window.location.assign("{{url('tiket')}}?NOTIKET="+data[1]+"@"+data[2]);
                               
                        }else{
                            $('#modalnot').modal({backdrop: 'static', keyboard: false});
                            $('#foot1').show();
                            $('#foot2').hide();
                            $('#notifikasi').html(msg);
                        }
                        
                        
                    }
                });

        } 
    </script>
@endpush