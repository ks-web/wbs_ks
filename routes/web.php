<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/lapor', 'LaporController@index');
Route::get('/tiket', 'LaporController@index_tiket');
Route::get('/pantau', 'LaporController@index_pantau');
Route::get('/pos', 'LaporController@index_pos');
Route::get('/drop', 'LaporController@index_drop');
Route::get('/sms', 'LaporController@index_sms');
Route::get('/email', 'LaporController@index_email');
Route::post('/lapor/simpan', 'LaporController@simpan');

Route::get('/lapor/view_data', 'LaporController@view_data');
Route::get('/admin/cetak/{id}', 'LaporController@cetak');
    
Route::group(['middleware'    => 'auth'],function(){
    Route::get('/admin', 'LaporController@index_admin');
    Route::get('/admin/view_data', 'LaporController@view_data_admin');
    Route::get('/admin/proses/{id}', 'LaporController@proses');
    Route::get('/admin/keputusan/{id}', 'LaporController@keputusan');
    Route::get('/admin/sanksi/{id}', 'LaporController@sanksi');
    Route::post('/admin/proses_investigasi', 'LaporController@proses_investigasi');
    Route::post('/admin/proses_keputusan', 'LaporController@proses_keputusan');
    Route::post('/admin/proses_sanksi', 'LaporController@proses_sanksi');

});